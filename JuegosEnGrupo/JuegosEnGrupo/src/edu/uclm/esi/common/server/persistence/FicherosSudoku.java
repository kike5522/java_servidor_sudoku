package edu.uclm.esi.common.server.persistence;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FicherosSudoku {
    

    
    
    public static String CargarSudokus(int indice){
        Scanner leer;
        String sudoku = null;
        
        try {
            leer = new Scanner(new File("sudoku"+indice+".txt")); //$NON-NLS-1$
            while(leer.hasNext()){
                sudoku=leer.nextLine();
            }
            leer.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        
        }
    return sudoku;
    }
    public static String SolucionSudokus(int indice){
        Scanner leer;
        String solucion=null;
    
        try {
            leer = new Scanner(new File("sudokus"+indice+".txt")); //$NON-NLS-1$
            while(leer.hasNext()){
                solucion=leer.nextLine();
                
            }
            leer.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        
        }
    return solucion;
    }
}
