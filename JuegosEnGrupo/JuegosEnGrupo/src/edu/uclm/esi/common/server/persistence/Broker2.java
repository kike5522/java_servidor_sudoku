package edu.uclm.esi.common.server.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

public class Broker2 {

    public final String GOOGLE_PWD = "JugadorGoogle35";
    private static Broker2 yoP;
    private String url;
    
    
    private int indice=0;

    public Broker2() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.url="jdbc:mysql://localhost:3306/records?noAccessToProcedureBodies=true";
        } catch (ClassNotFoundException e) {
            System.out.println(e.toString());
        }
    }

    public static Broker2 get() {
        if (yoP==null)
            yoP=new Broker2();
        return yoP;
    }

    public Connection getDBPrivilegiada() throws SQLException {
        return DriverManager.getConnection(url, "root", "");
    }

   // @SuppressWarnings("null")
    public String getSudokuSolucion() throws SQLException {
        String  resultado = null;
       
        Connection dbb=getDBPrivilegiada();

        try{
            String SQL="Select sudokuSolucion from sudoku where idSudoku=?";
            PreparedStatement pp=dbb.prepareStatement(SQL);
            pp.setInt(1, this.indice);
            ResultSet rr=pp.executeQuery();
            if(rr.next()){
                resultado=rr.getString(1);
            }
            rr.close();
        }
        catch (SQLException e) {
            throw e;
        }
        finally {
            dbb.close();
        }

        return resultado;
    }
    
    public String getSudoku() throws SQLException {
        String  resultado = null;
        Random re = new Random();
        this.indice=re.nextInt(7)+1;
        Connection dbb=getDBPrivilegiada();

        try{
            String SQL="Select sudoku from sudoku where idSudoku=?";
            PreparedStatement pp=dbb.prepareStatement(SQL);
            pp.setInt(1, this.indice);
            ResultSet rr=pp.executeQuery();
            if(rr.next()){
                resultado=rr.getString(1);
            }
            rr.close();
        }
        catch (SQLException e) {
            throw e;
        }
        finally {
            dbb.close();
        }

        return resultado;
    }
    
    public void actualizarTiempo(String email, long tiempo) throws SQLException {
       
        Connection dbb=getDBPrivilegiada();

        try{
            String SQL="INSERT INTO RECORDS (idPersona, tiempo, emailUsuario) VALUES (?, ?, ?)";
            PreparedStatement pp=dbb.prepareStatement(SQL);
            pp.setString(1, null);
            pp.setLong(2, tiempo);
            pp.setString(3, email);
            pp.executeUpdate();
           
        }
        catch (SQLException e) {
            throw e;
        }
        finally {
            dbb.close();
        }

       
    }
    
    public String[] getRanking() throws SQLException {
        String [] resultado = new String[2];
        int cont=0;
        Connection dbb=getDBPrivilegiada();

        try{
            String SQL="SELECT * FROM records ORDER BY tiempo ASC LIMIT 2";
            PreparedStatement pp=dbb.prepareStatement(SQL);
            //pp.setInt(1, this.indice);
            ResultSet rr=pp.executeQuery();
            while(rr.next()){
                resultado[cont]=""+rr.getLong(2)+" "+rr.getString(3);
                cont++;
            }
            rr.close();
        }
        catch (SQLException e) {
            throw e;
        }
        finally {
            dbb.close();
        }

        return resultado;
    }    
    

}