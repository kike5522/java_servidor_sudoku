package edu.uclm.esi.common.server.actions;

import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.uclm.esi.common.jsonMessages.ErrorMessage;
import edu.uclm.esi.common.jsonMessages.JSONMessage;
import edu.uclm.esi.common.jsonMessages.MessageList;
import edu.uclm.esi.common.server.domain.Manager;

@SuppressWarnings("serial")
public class GetMensajes extends JSONAction {
	private String email;
	private Vector<JSONMessage> mensajes;
	@Override
	protected String postExecute() { //lista de mensajes para el usuario
		this.mensajes=Manager.get().getMensajesPendientes(this.email); 
		
		return SUCCESS;
	}
	
	@Override
	public void setCommand(String cmd){
		JSONObject json;
		try{
			json=new JSONObject(cmd);
			this.email=json.get("email").toString();
			
		}catch(Exception e){
			this.exception=e;
		}
	}
	
	@Override

	public String getResultado() {

		if (this.exception!=null) {

			JSONMessage resultado=new ErrorMessage(this.exception.getMessage());

			return resultado.toString();

		} else { 

			MessageList ml=new MessageList();

			for (JSONMessage mensaje : this.mensajes) {

				ml.add(mensaje.toJSONObject());

			}

			mensajes.clear();

			String s=ml.toString();

			return s;

		}

	}
}

