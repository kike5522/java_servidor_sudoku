package edu.uclm.esi.common.jsonMessages;

import org.apache.struts2.json.JSONException;
import org.json.JSONObject;

public class SudokuMovementAnnouncementMessageRecibido extends JSONMessage{

    @JSONable
    private int row;
    @JSONable
    private int col;
    @JSONable
    private int value;
    @JSONable
    private int user;
    @JSONable
    private int idMatch;

    public SudokuMovementAnnouncementMessageRecibido(int value, int col, int row, int user, int idMatch) {
        super(true);
        this.value = value;
        this.col = col;
        this.row = row;
        this.user=user;
        this.idMatch=idMatch;
    }

    public SudokuMovementAnnouncementMessageRecibido(JSONObject jso) throws JSONException, NumberFormatException, org.json.JSONException {
        this(Integer.parseInt(jso.get("row").toString()), Integer.parseInt(jso.get("col").toString()), Integer.parseInt(jso.get("value").toString()), Integer.parseInt(jso.getString("user").toString()), Integer.parseInt(jso.getString("idMatch").toString()));
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public int getValue() {
        return value;
    }

    public int getUser(){ return user; }

    public int getIdMatch(){ return idMatch; }
}
