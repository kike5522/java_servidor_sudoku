package edu.uclm.esi.common.jsonMessages;
import org.json.JSONException;
import org.json.JSONObject;

public class LoseMessage extends JSONMessage{
    @JSONable
   
    private String emailGanador;
   
    
    public LoseMessage(String emailGanador) {
        super(true);
        
        this.emailGanador=emailGanador;
       
       
    }


    public LoseMessage(JSONObject jso) throws JSONException {
        this(jso.get("emailGanador").toString());
        
    }

    public String getEmailGanador() {
		return emailGanador;
	}


	   
}
