package com.sudokus.dominio;

public class Tablero {

	private String casillas;
	
	public Tablero(String casillas) {
		super();
		this.casillas = casillas;
	}

	public void actualizar(int row, int col, int value){
		
		 String resultado="";
         int posicion=(row*9)+col; 
         
         resultado=this.casillas.substring(0,posicion);
         resultado=resultado + value;
         resultado=resultado + this.casillas.substring(posicion+1,this.casillas.length());
         
		this.casillas=resultado;
	}
	
	public boolean terminado(String casillasResuelta){
		boolean terminado;
		if(this.casillas.equals(casillasResuelta))
			terminado = true;
		else
			terminado =  false;
		return terminado;
	}
	
	public String getCasillas() {
		return casillas;
	}
	
	public void setCasillas(String casillas) {
		this.casillas = casillas;
	}
	

}
