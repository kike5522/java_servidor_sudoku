package com.sudokus.dominio;

import java.io.IOException;
import java.util.Random;
import java.sql.SQLException;
import org.json.JSONException;
import org.json.JSONObject;

import com.maco.juegosEnGrupo.server.dominio.Game;
import com.maco.juegosEnGrupo.server.dominio.Match;

import edu.uclm.esi.common.jsonMessages.JSONMessage;
import edu.uclm.esi.common.jsonMessages.LoseMessage;
import edu.uclm.esi.common.jsonMessages.SudokuBoardMessage;
import edu.uclm.esi.common.jsonMessages.SudokuComparation;
import edu.uclm.esi.common.jsonMessages.SudokuMovementAnnouncementMessage;
import edu.uclm.esi.common.jsonMessages.SudokuMovementAnnouncementMessageRecibido;
import edu.uclm.esi.common.jsonMessages.SudokuMovementMessage;
import edu.uclm.esi.common.jsonMessages.SudokuWinnerMessage;
import edu.uclm.esi.common.server.domain.User;
import edu.uclm.esi.common.server.persistence.FicherosSudoku;
import edu.uclm.esi.common.server.domain.Manager;
import edu.uclm.esi.common.server.persistence.Broker2;

public class Sudoku extends Match  {


    String casillas;
    String casillasResuelta;
    
	  private Tablero tableroA, tableroB;
	  private long totalTiempoA;
      private long tiempoInicioA;
      
      private long totalTiempoB;
      private long tiempoInicioB;
      
      Broker2 brokerPropio = Broker2.get();
	  public Sudoku(Game game) {
	    super(game);
	    // TODO Auto-generated constructor stub
	  }

	  @Override
	  protected void postAddUser(User user) throws SQLException { 
		 
	      

		 //this.casillas=brokerPropio.getSudoku();
		 //this.casillasResuelta=brokerPropio.getSudokuSolucion();
	     this.casillas="011111111111111111111111111111111111111111111111111111111111111111111111111111111";
	     this.casillasResuelta="111111111111111111111111111111111111111111111111111111111111111111111111111111111";

	      if (this.players.size()==2) {
	          try {
	            User a = this.players.get(0);
	            User b = this.players.get(1);

	            JSONMessage jsBoardA = new SudokuBoardMessage(this.casillas, a.getEmail(), b.getEmail(), this.hashCode());
	            a.addMensajePendiente(jsBoardA);

	            JSONMessage jsBoardB = new SudokuBoardMessage(this.casillas, b.getEmail(), a.getEmail(), this.hashCode());
	            b.addMensajePendiente(jsBoardB);
	            tiempoInicioA = System.currentTimeMillis();
	            tiempoInicioB= System.currentTimeMillis();
	            tableroA = new Tablero (this.casillas);
	            tableroB = new Tablero (this.casillas);
	            
	          } catch (Exception e) {
	            e.printStackTrace();
	          }
	        }   
	  }

	  @Override
	  public String toString() {
	    // TODO Auto-generated method stub
	    return null;
	  }

	  @Override
	  protected boolean isTheTurnOf(User user) {
	    // TODO Auto-generated method stub
	    return true;
	  }

	  @Override
	  protected void postMove(User user, JSONObject jsoMovement) throws Exception {
	    // TODO Auto-generated method stub
		  String [] ranking=null;
		  JSONMessage lose=null;
		  User contrario;
		  Manager manager=Manager.get();
		  if(jsoMovement.get("type").equals(SudokuMovementMessage.class.getSimpleName())){
			  User usuario = manager.findUserById(jsoMovement.getInt("idUser"));
			
			  if(usuario==this.players.get(0)){
				  contrario=this.players.get(1);
				  tableroA.actualizar(Integer.parseInt(jsoMovement.get("rowCeldaACambiar").toString()), Integer.parseInt(jsoMovement.get("colCeldaACambiar").toString()), Integer.parseInt(jsoMovement.get("numeroAPoner").toString()) );
				  if (tableroA.terminado(this.casillasResuelta)){
					  totalTiempoA = (System.currentTimeMillis() - tiempoInicioA)/1000;
					 
					 brokerPropio.actualizarTiempo(usuario.getEmail(), totalTiempoA);
					 ranking=brokerPropio.getRanking();
					 JSONMessage winner = new SudokuWinnerMessage(totalTiempoA, ranking[0], ranking[1]);
					 lose = new LoseMessage(usuario.getEmail());
					 usuario.addMensajePendiente(winner);
					
					 
				  }
				
			  }else{
				  contrario=this.players.get(0);
				  tableroB.actualizar(Integer.parseInt(jsoMovement.get("rowCeldaACambiar").toString()), Integer.parseInt(jsoMovement.get("colCeldaACambiar").toString()), Integer.parseInt(jsoMovement.get("numeroAPoner").toString()) );
				  if (tableroB.terminado(this.casillasResuelta)){
					  totalTiempoB = (System.currentTimeMillis() - tiempoInicioB)/1000;
						 
						 brokerPropio.actualizarTiempo(usuario.getEmail(), totalTiempoB);
						 ranking=brokerPropio.getRanking();
						 JSONMessage winner = new SudokuWinnerMessage(totalTiempoB, ranking[0], ranking[1]);
						 lose = new LoseMessage(usuario.getEmail());
						 usuario.addMensajePendiente(winner); 
						 
				  }
			  	 
			  }
			  JSONMessage msn = new SudokuMovementAnnouncementMessage(jsoMovement.getInt("numeroAPoner"), jsoMovement.getInt("colCeldaACambiar"), jsoMovement.getInt("rowCeldaACambiar"), usuario.getId());
			  contrario.addMensajePendiente(msn);
			  contrario.addMensajePendiente(lose);
		  }

	  }
	 
	  /*
	  protected void compararSudokus(User user, JSONObject jsoMovement) throws Exception {
		  // TODO Auto-generated method stub
		  Manager manager=Manager.get();
		  if(jsoMovement.get("type").equals(SudokuComparation.class.getSimpleName())){
			  User usuario = manager.findUserById(jsoMovement.getInt("iduser"));
			  
			  if(jsoMovement.equals(this.casillasResuelta)){
				  JSONMessage msn = new SudokuWinnerMessage(jsoMovement.getLong("time"));
				  usuario.addMensajePendiente(msn);
				  //para Record
			  }
			  	  
		  }
	  }
	  */

	  @Override
	  protected void updateBoard(int row, int col, JSONMessage result) throws JSONException, IOException {
	    // TODO Auto-generated method stub

	  }

	}