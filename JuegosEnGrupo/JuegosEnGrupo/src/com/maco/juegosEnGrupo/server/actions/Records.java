package com.maco.juegosEnGrupo.server.actions;

import org.json.JSONException;
import org.json.JSONObject;

import com.maco.juegosEnGrupo.server.dominio.Game;
import com.maco.juegosEnGrupo.server.dominio.Match;
import com.opensymphony.xwork2.ActionContext;

import edu.uclm.esi.common.jsonMessages.ErrorMessage;
import edu.uclm.esi.common.jsonMessages.JSONMessage;
import edu.uclm.esi.common.jsonMessages.JSONable;
import edu.uclm.esi.common.jsonMessages.OKMessage;
import edu.uclm.esi.common.jsonMessages.RecordMessage;
import edu.uclm.esi.common.server.actions.JSONAction;
import edu.uclm.esi.common.server.domain.Manager;
import edu.uclm.esi.common.server.domain.User;
import edu.uclm.esi.common.server.persistence.Broker2;

@SuppressWarnings("serial")
public class Records extends JSONAction {
	private String ranking1;
	private String ranking2;

	private JSONObject jsoMovement;
	
	@Override
	public String postExecute() {
		try {/*
			Broker2 brokerPropio = Broker2.get();
			String [] ranking=null;
			ranking=brokerPropio.getRanking();
			
			RecordMessage msn = new RecordMessage( ranking[0], ranking[1]);
			Manager manager=Manager.get();
			//User user=manager.findUserById(this.iduser);
			//if (user==null)
				//throw new Exception("Usuario no autenticado");
			Game g=manager.findGameById(3);
			//Match match=g.findMatchById(idMatch, iduser);
			//match.move(user, this.jsoMovement);*/
			return SUCCESS;
		} catch (Exception e) {
			this.exception=e; 
			ActionContext.getContext().getSession().put("exception", e);
			return ERROR;
		}
	}

	@Override
	public String getResultado() {
		try {
			Broker2 brokerPropio = Broker2.get();
			String [] ranking=null;
			ranking=brokerPropio.getRanking();
			
			RecordMessage msn = new RecordMessage( ranking[0], ranking[1]);
			return msn.toString(); 
		}catch(Exception e){
			this.exception = e;
			
			return ERROR;
		}
		
		/*JSONMessage jso;
		if (this.exception!=null)
			jso=new ErrorMessage(this.exception.getMessage());
		else
			jso=new OKMessage();
		return jso.toJSONObject().toString(); 
		//PONLO COMO ANTES, justo cunado has empezado a darle a control z*/
	}

	@Override
	public void setCommand(String cmd) {
		try {
			jsoMovement = new JSONObject(cmd);
			this.ranking1=(jsoMovement.get("ranking1").toString());
			this.ranking2=(jsoMovement.get("ranking2").toString());
			

			
		} catch (JSONException e) {
			this.exception=e;
		}
	}


    public String getRanking1(){
        return  ranking1;
    }
 
    
    public String getRanking2(){
        return  ranking2;
    }
	
}
