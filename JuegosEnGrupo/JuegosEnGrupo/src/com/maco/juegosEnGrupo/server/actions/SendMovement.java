package com.maco.juegosEnGrupo.server.actions;

import org.json.JSONException;
import org.json.JSONObject;

import com.maco.juegosEnGrupo.server.dominio.Game;
import com.maco.juegosEnGrupo.server.dominio.Match;
import com.opensymphony.xwork2.ActionContext;

import edu.uclm.esi.common.jsonMessages.ErrorMessage;
import edu.uclm.esi.common.jsonMessages.JSONMessage;
import edu.uclm.esi.common.jsonMessages.JSONable;
import edu.uclm.esi.common.jsonMessages.OKMessage;
import edu.uclm.esi.common.server.actions.JSONAction;
import edu.uclm.esi.common.server.domain.Manager;
import edu.uclm.esi.common.server.domain.User;

@SuppressWarnings("serial")
public class SendMovement extends JSONAction {
	private int row;
    private int col;
    private int value;
    private int iduser;
    private int idMatch;

	private JSONObject jsoMovement;
	
	@Override
	public String postExecute() {
		try {
			Manager manager=Manager.get();
			User user=manager.findUserById(this.iduser);
			if (user==null)
				throw new Exception("Usuario no autenticado");
			Game g=manager.findGameById(3);
			Match match=g.findMatchById(idMatch, iduser);
			match.move(user, this.jsoMovement);
			return SUCCESS;
		} catch (Exception e) {
			this.exception=e; 
			ActionContext.getContext().getSession().put("exception", e);
			return ERROR;
		}
	}

	@Override
	public String getResultado() {
		JSONMessage jso;
		if (this.exception!=null)
			jso=new ErrorMessage(this.exception.getMessage());
		else
			jso=new OKMessage();
		return jso.toJSONObject().toString();
	}

	@Override
	public void setCommand(String cmd) {
		try {
			jsoMovement = new JSONObject(cmd);
			this.row=Integer.parseInt(jsoMovement.get("rowCeldaACambiar").toString());
			this.col=Integer.parseInt(jsoMovement.get("colCeldaACambiar").toString());
			this.value=Integer.parseInt(jsoMovement.get("numeroAPoner").toString());
			this.iduser=Integer.parseInt(jsoMovement.get("idUser").toString());
			this.idMatch=Integer.parseInt(jsoMovement.get("idMatch").toString());

		} catch (JSONException e) {
			this.exception=e;
		}
	}

	
	public void setRow(int row) {
		this.row = row;
	}

	

	public void setCol(int col) {
		this.col = col;
	}

	

	public void setValue(int value) {
		this.value = value;
	}

	

	public void setIduser(int iduser) {
		this.iduser = iduser;
	}

	

	public void setJsoMovement(JSONObject jsoMovement) {
		this.jsoMovement = jsoMovement;
	}
	
	
}