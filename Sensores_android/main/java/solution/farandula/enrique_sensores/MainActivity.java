package solution.farandula.enrique_sensores;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import static solution.farandula.enrique_sensores.R.id.Start;

public class MainActivity extends AppCompatActivity {

    Button start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        start= (Button)findViewById(Start);

        start.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent start= new Intent (MainActivity.this, AccelerometerActivity.class);
                startActivity(start);
                finish();
            }
        });
    }
}
