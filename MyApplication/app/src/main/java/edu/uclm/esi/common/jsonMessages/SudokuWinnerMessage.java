package edu.uclm.esi.common.jsonMessages;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Lauri on 04/04/2016.
 */
public class SudokuWinnerMessage extends JSONMessage{
    @JSONable
    private long time;
    @JSONable
    private String ranking1;
    @JSONable
    private String ranking2;

    public SudokuWinnerMessage(long time, String  ranking1, String  ranking2) {
        super(true);
        this.time = time;
        this.ranking1=ranking1;
        this.ranking2=ranking2;

    }


    public SudokuWinnerMessage(JSONObject jso) throws JSONException {
        this(Long.parseLong(jso.get("time").toString()), jso.get("ranking1").toString(), jso.get("ranking2").toString());

    }

    public String getRanking1() {
        return ranking1;
    }


    public String getRanking2() {
        return ranking2;
    }


    public long getTime() {
        return time;
    }
}
