package edu.uclm.esi.common.jsonMessages;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Usuario on 03/05/2016.
 */
public class SudokuMovementAnnouncementMessage extends JSONMessage{

    @JSONable
    private int row;
    @JSONable
    private int col;
    @JSONable
    private int value;
    @JSONable
    private int user;

    public SudokuMovementAnnouncementMessage(int value, int col, int row, int user) {
        super(true);
        this.value = value;
        this.col = col;
        this.row = row;
        this.user=user;
    }

    public SudokuMovementAnnouncementMessage(JSONObject jso) throws JSONException {
        this(Integer.parseInt(jso.get("value").toString()), Integer.parseInt(jso.get("col").toString()), Integer.parseInt(jso.get("row").toString()), Integer.parseInt(jso.getString("user").toString()));
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public int getValue() {
        return value;
    }

    public int getUser(){ return user; }
}

