package edu.uclm.esi.common.jsonMessages;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Usuario on 03/05/2016.
 */
public class SudokuComparation extends JSONMessage{
    @JSONable
    private String board;
    @JSONable
    private int user;
    @JSONable
    private int idMatch;

    public SudokuComparation(String board, int user, int idMatch) {
        super(false);
        this.board = board;
        this.idMatch = idMatch;
        this.user = user;

    }

    public SudokuComparation(JSONObject jso) throws JSONException {
        this(jso.getString("board"), Integer.parseInt(jso.getString("user").toString()), Integer.parseInt(jso.get("idMatch").toString()));
    }



    public int getIdMatch() {
        return idMatch;
    }

    public int getUser() {
        return user;
    }

    public String getBoard() {
        return board;
    }
}
