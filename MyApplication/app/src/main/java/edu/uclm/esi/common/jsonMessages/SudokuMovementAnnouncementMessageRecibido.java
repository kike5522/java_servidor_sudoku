package edu.uclm.esi.common.jsonMessages;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Lauri on 04/04/2016.
 */
public class SudokuMovementAnnouncementMessageRecibido extends JSONMessage{

    @JSONable
    private int row;
    @JSONable
    private int col;
    @JSONable
    private int value;
    @JSONable
    private int idUser;
    @JSONable
    private int idMatch;

    public SudokuMovementAnnouncementMessageRecibido(int value, int col, int row, int iduser, int idMatch) {
        super(true);
        this.value = value;
        this.col = col;
        this.row = row;
        this.idUser =iduser;
        this.idMatch=idMatch;
    }

    public SudokuMovementAnnouncementMessageRecibido(JSONObject jso) throws JSONException {
        this(Integer.parseInt(jso.get("row").toString()), Integer.parseInt(jso.get("col").toString()), Integer.parseInt(jso.get("value").toString()), Integer.parseInt(jso.getString("idUser").toString()), Integer.parseInt(jso.getString("idMatch").toString()));
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public int getValue() {
        return value;
    }

    public int getIdUser(){ return idUser; }

    public int getIdMatch(){ return idMatch; }
}
