/*package com.maco.clientejuegos.gui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.maco.clientejuegos.R;
import com.maco.clientejuegos.domain.Store;
import com.maco.clientejuegos.http.MessageRecoverer;
import com.maco.clientejuegos.http.NetTask;

import edu.uclm.esi.common.jsonMessages.JSONMessage;
import edu.uclm.esi.common.jsonMessages.JoinGameMessage;
import edu.uclm.esi.common.jsonMessages.LoginMessageAnnouncement;
import edu.uclm.esi.common.jsonMessages.SudokuBoardMessage;

//import com.example.usuario.myapplications.R;


public class WaitingAreaActivity extends AppCompatActivity implements IMessageDealerActivity {
    private LinearLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting_area);
        this.layout=(LinearLayout) findViewById(R.id.layoutWA);
        Store.get().setCurrentContext(this);
        Store store= Store.get();
        // Store.get().lanzarRecuperadorDeMensajes(this); // esto lo hemos quitado, por que ya no usamos en esta parte el estore, direcamente vamos a joingamemessage

        MessageRecoverer messageRecoverer = MessageRecoverer.get(this);
        messageRecoverer.setActivity(this);

        Thread t= new Thread(messageRecoverer);// para hacer un hilo, la clase que ponemos en el thread debe ser runnable
        t.start();

        /// creamos el join game
        JoinGameMessage jgm=new JoinGameMessage(store.getUser().getIdUser(), 3);
        NetTask task= new NetTask("JoinGame.action",jgm);
        task.execute();
    }

    @Override
    public void showMessage(JSONMessage jsm) {
        if(jsm.getType().equals(LoginMessageAnnouncement.class.getSimpleName())) {
            TextView tv = new TextView(this);
            tv.setText("Ha llegado" + ((LoginMessageAnnouncement) jsm).getEmail());
            this.layout.addView(tv);
        }
        if(jsm.getType().equals(SudokuBoardMessage.class.getSimpleName())){
            SudokuBoardMessage sbm=(SudokuBoardMessage) jsm;
            String casillas=sbm.getBoard();
            Store.get().setMatch(sbm.getIdMatch());
            Intent intent=new Intent(this, PartidaActivity.class);
            intent.putExtra("board", casillas);
            intent.putExtra("jugador1", sbm.getUser1());
            intent.putExtra("jugador2", sbm.getUser2());
            startActivity(intent);
        }
    }
}*/

package com.maco.clientejuegos.gui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.maco.clientejuegos.R;
import com.maco.clientejuegos.domain.Store;
import com.maco.clientejuegos.http.MessageRecoverer;
import com.maco.clientejuegos.http.NetTask;

import edu.uclm.esi.common.jsonMessages.JSONMessage;
import edu.uclm.esi.common.jsonMessages.JoinGameMessage;
import edu.uclm.esi.common.jsonMessages.LoginMessageAnnouncement;
import edu.uclm.esi.common.jsonMessages.SudokuBoardMessage;

public class WaitingAreaActivity extends AppCompatActivity implements IMessageDealerActivity {
    private LinearLayout layout;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting_area);
        this.layout=(LinearLayout) findViewById(R.id.layoutWA);//donde se le pone id a la vista abajo donde te marco, pone text view
        Store store=Store.get();
        store.setCurrentContext(this);
        MessageRecoverer messageRecoverer=MessageRecoverer.get(this);
        messageRecoverer.setActivity(this);
        Thread t=new Thread(messageRecoverer);
        t.start();
    }
    @Override
    protected void onResume() {
        super.onResume();
        JoinGameMessage jgm=new JoinGameMessage(Store.get().getUser().getIdUser(), 3); // 3 es el id de los sudokus

        NetTask task=new NetTask("JoinGame.action", jgm);
        task.execute();
    }
    @Override
    public void showMessage(JSONMessage jsm) {
        if (jsm.getType().equals(LoginMessageAnnouncement.class.getSimpleName())) {
            TextView tv=new TextView(this);
            tv.setText("Ha llegado " + ((LoginMessageAnnouncement) jsm).getEmail());
            this.layout.addView(tv);
        }
        if (jsm.getType().equals(SudokuBoardMessage.class.getSimpleName())) {
            SudokuBoardMessage sbm=(SudokuBoardMessage) jsm;
            String casillas=sbm.getBoard();
            Store.get().setMatch(sbm.getIdMatch());
            Intent intent=new Intent(this, PartidaActivity.class);
            intent.putExtra("board", casillas);
            intent.putExtra("jugador1", sbm.getUser1());
            intent.putExtra("jugador2", sbm.getUser2());
            startActivity(intent);
        }
    }


}
