package com.maco.clientejuegos.gui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;

import com.maco.clientejuegos.http.MessageRecoverer;

import edu.uclm.esi.common.jsonMessages.JSONMessage;
import edu.uclm.esi.common.jsonMessages.LoseMessage;
import edu.uclm.esi.common.jsonMessages.SudokuMovementAnnouncementMessage;
import edu.uclm.esi.common.jsonMessages.SudokuWinnerMessage;


public class PartidaActivity extends AppCompatActivity implements IMessageDealerActivity {
    private PartidaView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        ScreenParameters.screenWidth = dm.widthPixels;
        ScreenParameters.screenHeight = dm.heightPixels;

        MessageRecoverer.get(this).setActivity(this);

        view=new PartidaView(this);
        String board=getIntent().getStringExtra("board");
        view.setBoard(board);
        this.setContentView(view);
    }

    @Override
    public void showMessage(JSONMessage jsm) {
        if (jsm.getType().equals(SudokuMovementAnnouncementMessage.class.getSimpleName())) {
            SudokuMovementAnnouncementMessage smam=(SudokuMovementAnnouncementMessage) jsm;
            view.setCasilla(smam.getRow(), smam.getCol(), smam.getValue());
        }
        if (jsm.getType().equals(SudokuWinnerMessage.class.getSimpleName())) {
            SudokuWinnerMessage swm=(SudokuWinnerMessage) jsm;
            view.showVictory(swm);
        }
        showMessageI(jsm);

    }


    public void showMessageI(JSONMessage jsm) {

        if (jsm.getType().equals(LoseMessage.class.getSimpleName())) {
            LoseMessage lose=(LoseMessage) jsm;
            view.showDerrota(lose);
        }

    }


}